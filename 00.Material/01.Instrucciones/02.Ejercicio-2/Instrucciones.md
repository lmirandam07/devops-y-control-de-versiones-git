# Ejercicio 2
Comandos que utilizaremos: [Manual](Manual.md)

## Hagamos un cambio en el repositorio remoto
* Ingresemos a la siguiente ruta [https://gitlab.com/snippets/1898537](https://gitlab.com/snippets/1898537) y descargamos la clase ```SayayinDTO.java``` que usaremos como ejemplo para este ejercicio.
* Ahora subiremos el archivo recien descargado:
    * Nos vamos a la sección de ramas del repositorio. 
	* Ingresamos a la ruta ```01.Ejercicios```. 
	* Hacemos clic en el símbolo de "+" al lado de la ruta del repositorio.
	* En la sección de "This Directory" hacemos clic en "Upload file". 
	* Buscamos la clase que descargamos y hacemos clic en "Upload file"

## Fetch y Pull actualizando mi repositorio local
* A continuación volvemos a nuestra consola y revisaremos que si el respositorio fue actualizado. Para realizar esto usaremos el comando git fetch

```
git fetch
```

Nos saldrá algo parecido a esto si detecta cambios en el repositorio:

```
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 4 (delta 1), reused 3 (delta 0)
Unpacking objects: 100% (4/4), done.
From [URL_REPOSITORIO]
   dc1b3f0..c0c6ac8  dev        -> origin/dev
```

* Procederemos a realizar un git pull para descargar estos cambios

```
git pull
```

La salida de este comando nos mostrará los archivo(s) modificado(s):

```
Updating dc1b3f0..c0c6ac8
Fast-forward
 01.Ejercicios/SayayinDTO.java | 29 +++++++++++++++++++++++++++++
 1 file changed, 29 insertions(+)
 create mode 100644 01.Ejercicios/SayayinDTO.java
```

De esta manera podemos mantener nuestro repositorio local con los cambios que se han ido dando en los servidores remotos. Esta modificaciones pueden haber sido dada por 1 o más usuarios diferentes o por una misma persona en diferentes repositorios locales (o máquinas).