# Ejercicio 0
Comandos que utilizaremos: [Manual](Manual.md)

## Creando nuestro primer repositorio local
* Como primer punto haremos clic secundario en cualquier parte limpia del escritorio y haremos clic sobre la opción ```Git Bash Here```
* Con la consola que acaba de abrir haremos una nueva carpeta donde colocaremos todos los ejercicios praticos que haremos e inicializaremos nuestros primer repositorio git, para eso utilizamos las siguientes sentencias.
```
mkdir curso-de-git
cd curso-de-git
git init
```
* Dentro crearemos otra carpeta la cual nombraremos ```01.Ejercicios```
* Moviendonos a esta carpeta crearemos 2 archivos. Uno de ellos README.md tendrá una linea que dirá "Bienvenido a mi primer repositorio" (Puede abrirlo con su editor favorito, notepad, por ejemplo) el otro será llamado console.log y simulará ser un log de una aplicación el cual no queremos que se versione dentro de git.
```
touch README.md
touch console.log
```
* Crearemos un archivo llamado ```.gitignore``` (ojo no tiene nombre, sólo extensión) y procederemos a indicarle que ignore todos los archivos con extensión ```*.log```
```
touch .gitignore
```

Copiar la siguiente linea dentro del archivo ```.gitignore```
```
*.log
```